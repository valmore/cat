//Powered By if, Since 2014 - 2020

package com.zs.pig.[model].mapper;

import com.github.abel533.mapper.Mapper;
import com.zs.pig.[model].api.model.[entityClass];


/**
 * 
 * @author [author] [date]
 * @Email: 951449465@qq.com
 * @version 4.0v
 *	[description]
 */
public interface [entityClass]Mapper extends Mapper<[entityClass]>{

}
